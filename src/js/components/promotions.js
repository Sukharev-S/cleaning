import Swiper from '../vendor/swiper-bundle.min'

if (document.querySelector('.promotions')) {
    if (document.querySelector('.swiper-container')) {
        let swiperStretch = window.innerWidth < 576 ? 100 : 300,
            sliderSelector = '.swiper-container',
            options = {
                initialSlide: 1,
                init: false,
                loop: false,
                speed: 1500,
                slidesPerView: 1,
                spaceBetween: 0,
                centeredSlides: true,
                effect: 'coverflow',
                coverflowEffect: {
                    rotate: 45,
                    stretch: swiperStretch,
                    depth: 550,
                    modifier: 1,
                    slideShadows: true,
                },
                grabCursor: true,
                parallax: true,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
                breakpoints: {
                    1650: {
                        slidesPerView: 2.2,
                        spaceBetween: 0
                    },
                    1500: {
                        slidesPerView: 2,
                        spaceBetween: 0
                    },
                    1200: {
                        slidesPerView: 1.75,
                        spaceBetween: 0
                    },
                    320: {
                        slidesPerView: 'auto',
                        spaceBetween: 0
                    }
                }
            }

        let mySwiper = new Swiper(sliderSelector, options)
        mySwiper.init()
    }
}