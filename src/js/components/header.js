import {modalWindowOff, modalWindowOn} from "../functions/modal-window";

const
    covid = document.querySelector('.header__covid'),
    covidModal = document.querySelector('.covid-modal'),
    covidClose = document.querySelector('.covid-modal .modal__close')

covid.addEventListener('click', e => {
    modalWindowOn(covidModal, 'covid-modal--visible')
})

covidClose.addEventListener('click', e => {
    modalWindowOff(covidModal, 'covid-modal--visible')
})

