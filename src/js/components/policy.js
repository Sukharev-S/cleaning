import {modalWindowOff, modalWindowOn} from "../functions/modal-window";

const
    policyButtons = document.querySelectorAll('.js-policy-ref'),
    policyModal = document.querySelector('.policy-modal'),
    policyClose = document.querySelector('.policy-modal .modal__close')

policyButtons.forEach(el => {
    el.addEventListener('click', e => {
        e.preventDefault()
        modalWindowOn(policyModal, 'policy-modal--visible')
    })
})

policyClose.addEventListener('click', e => {
    modalWindowOff(policyModal, 'policy-modal--visible')
})