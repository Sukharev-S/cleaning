
if (document.querySelector('.faq')) {
    const faqItems = document.querySelectorAll('.faq__item-header')

    faqItems.forEach(el => {
        el.addEventListener('click', e => {
            let currentItemAnswer = e.currentTarget.closest('.faq__item').querySelector('.faq__item-answer')
            let currentItemArrow = e.currentTarget.querySelector('.faq__item-arrow')

            if (getComputedStyle(currentItemAnswer).height === "0px") { // если ответ скрыт
                gsap.to(currentItemAnswer, {
                    duration: 1,
                    height: 'auto',
                    ease: "elastic.out(1, 0.4)",
                })
                gsap.to(currentItemArrow, {
                    duration: 1,
                    rotate: 0,
                    ease: "elastic.out(1, 0.5)",
                })
            } else {
                gsap.to(currentItemAnswer, {         // если ответ показан
                    duration: 0.5,
                    height: 0,
                    ease: "sine.out",
                })
                gsap.to(currentItemArrow, {
                    duration: 0.5,
                    rotate: -90,
                    ease: "sine.out",
                })
            }
        })
    })
}