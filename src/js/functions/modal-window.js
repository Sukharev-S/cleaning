const
    overlay = document.querySelector('.overlay'),
    covidModal = document.querySelector('.covid-modal'),
    policyModal = document.querySelector('.policy-modal'),
    header = document.querySelector('.header'),
    callMeBtn = document.querySelector('.callback-btn')

let callMeBtnRight = 0  // исходное значение позиции right мобильной кнопки ПЕРЕВОНИТЬ

// -------  общая функция открытия модального окна с расчетом ширины скролла при body.overflow
export const modalWindowOn = function(element, animateClass) {
    let bodyWidthBefore = document.body.clientWidth
    element.classList.add(animateClass)
    overlay.classList.add('overlay--visible')
    document.body.style.overflow = 'hidden'
    document.body.style.paddingRight = document.body.clientWidth - bodyWidthBefore + 'px'
    header.style.paddingRight = document.body.style.paddingRight
    callMeBtnRight = getComputedStyle(callMeBtn).right
    callMeBtn.style.right = parseInt(callMeBtnRight) + parseInt(document.body.style.paddingRight) + "px"
}

// -------  общая функция закрытия модального окна с расчетом ширины скролла при body.overflow
export const modalWindowOff = function(element, animateClass) {
    element.classList.remove(animateClass)
    overlay.classList.remove('overlay--visible')
    setTimeout(() => {  // для устранения рывков при удалении паддингов
        document.body.style.overflow = 'visible'
        document.body.style.paddingRight = '0'
        header.style.paddingRight = '0'
        callMeBtn.style.right = callMeBtnRight
    }, 100)
}

// -------  функция для обработчиков закрытия модальных окон
const closeModalWindows = function() {
    modalWindowOff(covidModal, 'covid-modal--visible')
    modalWindowOff(policyModal, 'policy-modal--visible')
}

// -------  обработчик клика на overlay (для модальных окон)
overlay.addEventListener('click', e => {
    closeModalWindows()
})

// -------  обработчик закрытия модальных окон по нажатию ESCAPE
document.body.addEventListener('keydown', e => {
    if (e.code  === 'Escape') {
        closeModalWindows()
    }
})