<?php

    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;

    require 'PHPMailer-6.5.0/src/PHPMailer.php';
    require 'PHPMailer-6.5.0/src/Exception.php';

    $name = '';
    $phone = '';
    $service = '';
    $novice = '';
    $totalPrice = '';
    $space = '';
    $services = [
        'maintenance_cleaning' => 'поддерживающая уборка',
        'general_cleaning' => 'генеральная уборка',
        'cleaning_up_after_renovation' => 'уборка после ремонта',
        'steam_cleaner' => 'обработка паром',
        'washing_windows' => 'мытье окон',
        'additional_services' => 'дополнительные услуги',
    ];
    $works = [
        'mosquito_nets' => 'Мытье москитных сеток',
        'glass_partitions_one_side' => 'Мытье стеклянных перегородок с ОДНОЙ стороны',
        'glass_partitions_two_side' => 'Мытье стеклянных перегородок с ДВУХ сторон',
        'windows_after_renovation' => 'Мытье окон после ремонта',
        'two_seater_sofa' => 'Диван 2х местный',
        'armchair' => 'Кресло',
        'tree_seater_sofa' => 'Диван 3х местный',
        'corner_sofa' => 'Диван угловой',
        'multi_seat_sofa' => 'Диван многоместный',
        'sofa_bed_inside' => 'Диван-кровать +внутри',
        'chair_bed_inside' => 'Кресло-кровать +внутри',
        'double_mattress' => 'Матрас двуспальный',
        'chair' => 'Стул/пкф',
        'carpet' => 'Ковролин/ковер',
        'stuffed_toys' => 'Мягкие игрушки',
        'tulle_curtains' => 'Шторы/тюль',
        'radiator_treatment' => 'Обработка радиатора'
    ];

    // функция очищает от ненужных символов интерактивные поля
    function cleaner ($value = "") : ?string {
        $value = trim($value);
        $value = stripslashes($value);
        $value = strip_tags($value);
        $value = htmlspecialchars($value);
        return $value;
    }

    // обработка пришедших данных
    $name = cleaner($_POST['name']);
    $phone = cleaner($_POST['phone']);
    $service = cleaner($_POST['service']);
    $novice = cleaner($_POST['novice']);
    $space = cleaner($_POST['space']);
    $totalPrice = cleaner($_POST['totalPrice']);

    unset($_POST['token']);
    unset($_POST['name']);
    unset($_POST['phone']);
    unset($_POST['service']);
    unset($_POST['novice']);
    unset($_POST['space']);
    unset($_POST['totalPrice']);

    $service = $services[$service];
    $totalPrice = $totalPrice.' ₽';

    $mail = new PHPMailer(true);
    $mail -> CharSet = 'UTF-8';
    $mail -> setLanguage('ru', 'PHPMailer-6.5.0/language');
    $mail -> IsHTML(true);

    // -- От кого письмо
    $mail -> setFrom('info@дарьяклининг.рф', 'Дарья Клининг');

    // -- Кому
    $mail -> addAddress('daryacleaning@gmail.com');

    //  -- Тема письма
    $mail -> Subject = 'Поступил заказ услуг!';

    //  -- Тело письма
    if ($service === 'дополнительные услуги') {
        $body = '<h2>Поступил заказ на дополнительные услуги!</h2>';
        $body .= '<p><strong>Заказчик:</strong> '.$name.'</p>';
        $body .= '<p><strong>Телефон:</strong> '.$phone.'</p>';
    } else {
        $body = '<h2>Поступил заказ на услугу ' . $service . '!</h2>';
        $body .= '<p><strong>Заказчик:</strong> ' . $name . '</p>';
        $body .= '<p><strong>Телефон:</strong> ' . $phone . '</p>';
        if (!empty($novice)) {
            $body .= '<p><strong>Это первый заказ! </strong></p>';
        }
        if (!empty($space)) {
            $body .= '<p><strong>Заказана площадь уборки: </strong> ' . $space . ' м2</p>';
        }
        if (count($_POST) > 0) {
            $body .= '<p><strong>Выбраны услуги:</strong></p>';
            foreach ($_POST as $key => $value) {
                $body .= '<p>' . $works[$key] . ':______<strong>' . $value . '</strong></p>';
            }
        }
        $body .= '<p><strong>На сумму: </strong> ' . $totalPrice . '</p>';
    }

    $mail -> msgHTML($body);

    if (!$mail -> send()) {
        $message = 'error';
    } else {
        $message = 'success';
    }

    $response = $message;
    header('Content-type: application/json');
    echo json_encode($response);

