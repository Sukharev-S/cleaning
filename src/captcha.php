<?php

    if (isset($_POST)) {
        $captcha = $_POST["token"];
        $secretKey = '6Le9go4bAAAAAGjllwJTqFQrLI2Ni8CaRrlVotMv';
        $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . $secretKey . '&response=' . $captcha;

        $response = file_get_contents($url);
        $responseKeys = json_decode($response, true);
        header('content-type: application/json');
        if ($responseKeys["success"] && $responseKeys["score"] >= 0.5) {
            if (isset($_POST["service"])) {
                include_once('services-order.php');
            } else {
                include_once('call-me.php');
            }
    //        echo json_encode(array('success' => 'true', 'score' => $responseKeys["score"]));
        }
    }